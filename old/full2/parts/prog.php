<h2>Fish and Seals Simulation</h2>
<p>For a final project of the Apprenticeship program at Shodor, I worked with two other interns to create a predator and prey simulation of an ecosystem with fish and seals. It's based around the idea that some of the fish are marked and the other fish follow them - creating a simple version of schools of fish.</p>
<div style="text-align: center;"><img src="images/fishsealssim.jpg" style="max-width: 100%; height: auto;"></div>
<a style="float: right" href="http://shodorsite.andrewnyland.net/jsfishseals/">View here</a>