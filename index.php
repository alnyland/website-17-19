<!doctype html>
<html>
	<head>
		<title>AndrewNyland.net - Home</title>
        <meta name="referrer" content="origin">
		<link rel="stylesheet" href="/style.css" type="text/css">
		<script src="/script.js"></script>
        <script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create', 'UA-36511038-2', 'auto');ga('send', 'pageview');</script>
	</head>
	<body class="home">
		<div class="overlay home-page-wrap">
			<a href="/" class="nostyle"><h1 id="title">Andrew Nyland</h1></a>
			<div class="description">
				<a href="/photography" onmouseover="activate(this);" onmouseout="goaway(this);" class="color-0">Photographer<span class="desc-period">.</span></a>
				<a href="/programming" onmouseover="activate(this);" onmouseout="goaway(this);" class="color-1">Programmer<span class="desc-period">.</span></a>
				<a href="/designs" onmouseover="activate(this);" onmouseout="goaway(this);" class="color-2">Designer<span class="desc-period">.</span></a>
				<a href="/blog" onmouseover="activate(this);" onmouseout="goaway(this);" class="color-3">Writer<span class="desc-period">.</span></a>
				<a href="/about" onmouseover="activate(this);" onmouseout="goaway(this);" class="color-4">Student<span class="desc-period">.</span></a>
			</div>
		</div>
		<div id="home-overlay"></div>
		<div class="clearfix"></div>
	</body>
</html>
