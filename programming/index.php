<!doctype html>
<html>
	<head>
		<title>AndrewNyland.net - Programming</title>
        <meta name="referrer" content="origin">
		<script src="/script.js"></script>
		<link rel="stylesheet" type="text/css" href="/style.css">
	</head>
	<body class="color-1">
		<?php $title = "programming"; include("../header.php"); ?>
		<div class="page-wrap">
			<div class="prog-item prog-text">
				<h2>About</h2>
				<p>I began programming in 2012, and since then have taught myself how to program in languages such as python, C/C++, Swift, Javascript, PHP, and Java. I've also created projects with tools such AgentSheets, Vensim, and LabView (<a href="http://shodorsite.andrewnyland.net/">viewable here</a>). Recently my main projects have been:
					<ul>
						<li>graphical, AI, and biological simulations,</li>
						<li>a CMS based around the idea of an artificial filesystem in which the user can store text content with other types of content embedded,</li>
						<li>and a web app that combines WYSIWYG editors with proportional measurements capable of creating fully responsive websites with an interactive editor.</li>
					</ul>
					Below are listed my major recent completed projects.
				</p>
			</div>
			<div class="prog-item">
				<p data-height="300" data-theme-id="dark" data-slug-hash="EHaAt" data-default-tab="result" data-user="ALNyland" data-embed-version="2" class="codepen">See the Pen <a href="http://codepen.io/ALNyland/pen/EHaAt/">Binary Shapes</a> by Andrew Nyland (<a href="http://codepen.io/ALNyland">@ALNyland</a>) on <a href="http://codepen.io">CodePen</a>.</p>
				<script async src="//assets.codepen.io/assets/embed/ei.js"></script>
			</div>
			<div class="prog-item">
				<img src="images/fishsealssim.jpg" style="width: 70%; float: left;"/>
				<div class="prog-text" style="float: left; width: 25%; height: 100%;">
					<h3>Fish and seals simulation</h3>
					<p>For a final project of the Apprenticeship program at Shodor, I worked with two other interns to create a predator and prey simulation of an ecosystem with fish and seals. It's based around the idea that some of the fish are marked and the other fish follow them - creating a simple version of schools of fish.</p>
					<p>This simulation can be viewed <a href="http://shodorsite.andrewnyland.net/jsfishseals/">here</a>.</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="prog-item">
				<p data-height="402" data-theme-id="dark" data-slug-hash="CKELe" data-default-tab="js,result" data-user="ALNyland" data-embed-version="2" class="codepen">See the Pen <a href="http://codepen.io/ALNyland/pen/CKELe/">Orthographic 3D renderer</a> by Andrew Nyland (<a href="http://codepen.io/ALNyland">@ALNyland</a>) on <a href="http://codepen.io">CodePen</a>.</p>
				<script async src="//assets.codepen.io/assets/embed/ei.js"></script>
				<div class="prog-text">
					<h3>Orthographic 2D Renderer</h3>
				</div>
			</div>
			<div class="prog-item">
				<p data-height="265" data-theme-id="light" data-slug-hash="nIpFG" data-default-tab="result" data-user="ALNyland" data-embed-version="2" class="codepen">See the Pen <a href="http://codepen.io/ALNyland/pen/nIpFG/">Particles 1</a> by Andrew Nyland (<a href="http://codepen.io/ALNyland">@ALNyland</a>) on <a href="http://codepen.io">CodePen</a>.</p>
				<script async src="//assets.codepen.io/assets/embed/ei.js"></script>
				<div class="prog-text">
					<h3>Particle Generator</h3>
					<p><em>An early test - click it to stop the animation.</em></p>
				</div>
			</div>
		</div>
		<?php include("../footer.php");?>
	</body>
</html>
